package tech.spring.security.customfilters.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import tech.spring.security.customfilters.filters.RequestLoggingFilter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.httpBasic();
        http.addFilterBefore(new RequestLoggingFilter(), BasicAuthenticationFilter.class)
                .authorizeRequests().anyRequest().authenticated();
    }
}
