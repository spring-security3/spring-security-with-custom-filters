package tech.spring.security.customfilters.resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class GreetingResource {

    @GetMapping
    public String greet() {
        return "Hello from Spring Security with custom filters check";
    }
}
