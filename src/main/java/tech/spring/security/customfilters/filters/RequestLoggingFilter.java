package tech.spring.security.customfilters.filters;

import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

public class RequestLoggingFilter implements Filter {
    private static final Logger LOGGER = Logger.getLogger(RequestLoggingFilter.class.getName());

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;

        LOGGER.info(request.getRequestURI());
        LOGGER.info(request.getMethod());
        LOGGER.info(request.getQueryString());

        filterChain.doFilter(servletRequest, servletResponse);
    }
}
